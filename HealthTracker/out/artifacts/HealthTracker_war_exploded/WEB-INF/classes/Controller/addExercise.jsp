<%@ page import="Model.Exercise" %>
<%@ page import="Model.User" %><%--
  Created by IntelliJ IDEA.
  User: sebpa
  Date: 13/03/2018
  Time: 22:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<meta http-equiv="refresh" content="0; url=http://127.0.0.1:8080/dashboard.jsp">
<head>
    <title></title>
</head>
<body>


<%
    User currentUser = (User) session.getAttribute("currentUser");
    String exerciseType = request.getParameter("exerciseType");
    int duration = Integer.parseInt(request.getParameter("duration"));
    int calories = Integer.parseInt(request.getParameter("exerciseCalories"));

    Exercise newExercise = new Exercise(exerciseType, duration, calories);

    currentUser.getExerciseLog().add(newExercise);
%>
</body>
</html>
