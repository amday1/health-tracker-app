<%@ page import="Model.Food" %>
<%@ page import="Model.User" %>
<%@ page import="java.time.LocalDate" %><%--
  Created by IntelliJ IDEA.
  User: sebpa
  Date: 13/03/2018
  Time: 22:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<meta http-equiv="refresh" content="0; url=http://127.0.0.1:8080/dashboard.jsp">
<head>
    <title></title>
</head>
<body>


<%
    User currentUser = (User) session.getAttribute("currentUser");
    String foodName = request.getParameter("foodName");
    int calories = Integer.parseInt(request.getParameter("foodCalories"));

    Food newFood = new Food(foodName, calories, LocalDate.now());

    currentUser.getDietLog().add(newFood);
%>
</body>
</html>
