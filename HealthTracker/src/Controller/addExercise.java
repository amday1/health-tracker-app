package Controller;

import Model.CurrentUser;
import Model.Exercise;
import Model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.time.LocalDateTime;

@WebServlet(name = "addExercise", urlPatterns = {"/addExercise"})
public class addExercise extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {

            // Store the current signed in user in a variable
            User currentUser = CurrentUser.getCurrentUser();

            // Declare and assign the correct ID for the piece of exercise
            int exerciseID;
            if (!currentUser.getExerciseLog().isEmpty()) {
                // Add 1 to the ID of the previous piece of exercise
                exerciseID = currentUser.getExerciseLog().get(currentUser.getExerciseLog().size() - 1).getExerciseID() + 1;
            } else {
                // If its the first piece to be added then set its ID to be 1
                exerciseID = 1;
            }

            // Get the input from the form and store them
            String exerciseType = request.getParameter("exerciseType");
            int duration = Integer.parseInt(request.getParameter("duration"));
            int exerciseCalories = Integer.parseInt(request.getParameter("exerciseCalories"));

            // Create the new Exercise object
            Exercise newExercise = new Exercise(exerciseID, exerciseType, duration, exerciseCalories, LocalDateTime.now());

            // Add the new piece of exercise to the current users exercise log
            currentUser.getExerciseLog().add(newExercise);

            // Create an SQL array using the current users exercise log
            Array dbExercise = dbConnect.connect().createArrayOf("TEXT", Exercise.convertToStringArr());

            // Prepare and execute the SQL statement that will update the users exercise log
            PreparedStatement st = dbConnect.connect().prepareStatement("UPDATE userDB SET exercise = ? WHERE userName = ?");
            st.setArray(1, dbExercise);
            st.setString(2, currentUser.getUserName());
            st.executeUpdate();
            st.close();

            // Redirect back to the dashboard
            response.sendRedirect("/dashboard.jsp");

        } catch (Throwable e) {
            System.out.println(e);
        }
    }
}