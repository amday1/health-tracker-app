package Controller;

import Model.CurrentUser;
import Model.Food;
import Model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.time.LocalDateTime;

@WebServlet(name = "addFood", urlPatterns = {"/addFood"})
public class addFood extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {

            // Store the current signed in user in a variable
            User currentUser = CurrentUser.getCurrentUser();

            // Declare and assign the correct ID for the piece of food
            int foodID;
            if (!currentUser.getDietLog().isEmpty()) {
                // Add 1 to the ID of the previous piece of exercise
                foodID = currentUser.getDietLog().get(currentUser.getDietLog().size() - 1).getFoodID() + 1;
            } else {
                // If its the first piece to be added then set its ID to be 1
                foodID = 1;
            }

            // Get the input from the form and store them
            String name = request.getParameter("foodName");
            int calories = Integer.parseInt(request.getParameter("foodCalories"));

            // Create the new Food object
            Food newFood = new Food(foodID, name, calories, LocalDateTime.now());

            // Add the new piece of food to the current users diet log
            currentUser.getDietLog().add(newFood);

            // Create an SQL array using the current users diet log
            Array dbFood = dbConnect.connect().createArrayOf("TEXT", Food.convertToStringArr());

            // Prepare and execute the SQL statement that will update the users diet log
            PreparedStatement st = dbConnect.connect().prepareStatement("UPDATE userDB SET food = ? WHERE userName = ?");
            st.setArray(1, dbFood);
            st.setString(2, currentUser.getUserName());
            st.executeUpdate();
            st.close();

            // Redirect back to the dashboard
            response.sendRedirect("/dashboard.jsp");

        } catch (Throwable e) {
            System.out.println(e);
        }
    }
}