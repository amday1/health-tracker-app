package Controller;

import Model.CurrentUser;
import Model.Goal;
import Model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.time.LocalDateTime;

@WebServlet(name = "addGoal", urlPatterns = {"/addGoal"})
public class addGoal extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {

            // Store the current signed in user in a variable
            User currentUser = CurrentUser.getCurrentUser();

            // Declare and assign the correct ID for the goal
            int goalID;
            if (!currentUser.getGoals().isEmpty()) {
                // Add 1 to the ID of the previous goal
                goalID = currentUser.getGoals().get(currentUser.getGoals().size() - 1).getGoalID() + 1;
            } else {
                // If its the first goal to be added then set its ID to be 1
                goalID = 1;
            }

            // Get the input from the form and store them
            String theGoal = request.getParameter("newGoal");
            LocalDateTime dateAdded = LocalDateTime.now();

            // Create the new Goal object
            Goal newGoal = new Goal(goalID, theGoal, dateAdded, false);

            // Add the new piece of food to the current users goal list
            currentUser.getGoals().add(newGoal);

            // Create an SQL array using the current users goal list
            Array dbGoals = dbConnect.connect().createArrayOf("TEXT", Goal.convertToStringArr());

            // Prepare and execute the SQL statement that will update the users goal list
            PreparedStatement st = dbConnect.connect().prepareStatement("UPDATE userDB SET goals = ? WHERE userName = ?");
            st.setArray(1, dbGoals);
            st.setString(2, currentUser.getUserName());
            st.executeUpdate();
            st.close();

            // Redirect back to the dashboard
            response.sendRedirect("/dashboard.jsp");

        } catch (Throwable e) {
            System.out.println(e);
        }
    }
}