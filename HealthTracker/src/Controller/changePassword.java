package Controller;

import Model.CurrentUser;
import Model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@WebServlet(name = "changePassword", urlPatterns = {"/changePassword"})
public class changePassword extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {

            // Get the input from the form and store it
            String oldPassword = User.generateSHAPassword(request.getParameter("oldAccPassword"));
            String newPassword = User.generateSHAPassword(request.getParameter("newAccPassword"));

            // Old password must match the password in the database
            // AND the new password does not match the old password
            if (oldPassword.equals(CurrentUser.getCurrentUser().getPassword()) && !oldPassword.equals(newPassword)) {
                // Prepare and execute an SQL statement that will update the password for current user
                PreparedStatement st = dbConnect.connect().prepareStatement("UPDATE userdb SET pass = ? WHERE username = ?");
                st.setString(1, newPassword);
                st.setString(2, CurrentUser.getCurrentUser().getUserName());
                st.executeUpdate();
                st.close();

                // Redirect back to the dashboard
                response.sendRedirect("/dashboard.jsp");
            } else {
                // Print out an error message and refresh the page to inform the user
                String incorrectPassword = "Either the current password was entered incorrectly or the new password matched the old password";
                request.getSession().setAttribute("incorrectPassword", incorrectPassword);
                response.sendRedirect("/dashboard.jsp");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}