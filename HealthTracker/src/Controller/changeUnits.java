package Controller;

import Model.CurrentUser;
import Model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@WebServlet(name = "changeUnits", urlPatterns = {"/changeUnits"})
public class changeUnits extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {

            // Store the current signed in user in a variable
            User currentUser = CurrentUser.getCurrentUser();

            // Get the users current unit type
            boolean metric = currentUser.getMetric();

            // Set the current users unit type to the other one
            // Note: this automatically updates the height and weight for the signed in user
            currentUser.setMetric(!metric);

            // Prepare and execute the SQL statement that will update the users weight, height and unit type in the database
            PreparedStatement update = dbConnect.connect().prepareStatement("UPDATE userDB SET startweight = ?, currentweight = ?, height = ?, metric = ? WHERE userName = ?");
            update.setDouble(1, currentUser.getStartWeight());
            update.setDouble(2, currentUser.getCurrentWeight());
            update.setDouble(3, currentUser.getHeight());
            update.setBoolean(4, currentUser.getMetric());
            update.setString(5, currentUser.getUserName());
            update.executeUpdate();
            update.close();

            // Redirect back to the dashboard
            response.sendRedirect("/dashboard.jsp");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}