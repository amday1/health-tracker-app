package Controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class dbConnect {

    /**
     * A static method that connects to the database
     *
     * @return conn
     */
    public static Connection connect() {

        // Declare the connection variable and initialize it to null
        Connection conn = null;

        try {

            // Load the postgresql driver class at runtime
            Class.forName("org.postgresql.Driver");

            // Connect to the database
            conn = DriverManager.getConnection("jdbc:postgresql://healthtrackerdb.crgnzxr5ysub.eu-west-2.rds.amazonaws.com:5432/healthTrackerDB",
                    "ServerAdmin", "ServerPass");

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }
}