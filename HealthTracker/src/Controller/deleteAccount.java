package Controller;

import Model.CurrentUser;
import Model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@WebServlet(name = "deleteAccount", urlPatterns = {"/deleteAccount"})
public class deleteAccount extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {

            // Store the current signed in user in a variable
            User currentUser = CurrentUser.getCurrentUser();

            // If the hashed password from the input form is equal to the hashed password from the database
            if (User.generateSHAPassword(request.getParameter("deleteAccPassword")).equals(currentUser.getPassword())) {
                // Prepare and execute the SQL statement that will delete the user from the database
                PreparedStatement st = dbConnect.connect().prepareStatement("DELETE FROM userdb WHERE username = ?");
                st.setString(1, currentUser.getUserName());
                st.executeUpdate();
                st.close();

                // Sign the user out
                CurrentUser.setCurrentUser(null);

                // Redirect to the homepage
                response.sendRedirect("/");
            } else {
                // Print out an error message and refresh the page to inform the user
                String incorrectPassword = "The password you entered was incorrect! Try again!";
                request.getSession().setAttribute("incorrectPassword", incorrectPassword);
                response.sendRedirect("/dashboard.jsp");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
