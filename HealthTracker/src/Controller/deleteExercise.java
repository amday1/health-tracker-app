package Controller;

import Model.CurrentUser;
import Model.Exercise;
import Model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet(name = "deleteExercise", urlPatterns = {"/deleteExercise"})
public class deleteExercise extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {

            // Store the current signed in user in a variable
            User currentUser = CurrentUser.getCurrentUser();

            // Get the current users exercise log and store it
            ArrayList<Exercise> userExercise = currentUser.getExerciseLog();

            if (!userExercise.isEmpty()) {
                // When the food with the matching exercise ID is found then remove it
                for (int i = 0; i < userExercise.size(); i++) {
                    if (userExercise.get(i).getExerciseID() == Integer.parseInt(request.getParameter("exerciseID"))) {
                        userExercise.remove(i);
                    }
                }

                // Set the current users exercise log to be the modified version of the list
                currentUser.setExerciseLog(userExercise);

                // Create an SQL array using the current users exercise log
                Array dbFood = dbConnect.connect().createArrayOf("TEXT", Exercise.convertToStringArr());

                // Prepare and execute the SQL statement that will update the users exercise log
                PreparedStatement st = dbConnect.connect().prepareStatement("UPDATE userDB SET exercise = ? WHERE userName = ?");
                st.setArray(1, dbFood);
                st.setString(2, currentUser.getUserName());
                st.executeUpdate();
                st.close();


            }

                // Redirect back to the dashboard
                response.sendRedirect("/dashboard.jsp");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}