package Controller;

import Model.CurrentUser;
import Model.Food;
import Model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet(name = "deleteFood", urlPatterns = {"/deleteFood"})
public class deleteFood extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {

            // Store the current signed in user in a variable
            User currentUser = CurrentUser.getCurrentUser();

            // Get the current users diet log and store it
            ArrayList<Food> userFood = currentUser.getDietLog();

            if (!userFood.isEmpty()) {
                // When the food with the matching food ID is found then remove it
                for (int i = 0; i < userFood.size(); i++) {
                    if (userFood.get(i).getFoodID() == Integer.parseInt(request.getParameter("foodID"))) {
                        userFood.remove(i);
                    }
                }

                // Set the current users diet log to be the modified version of the list
                currentUser.setDietLog(userFood);

                // Create an SQL array using the current users diet log
                Array dbFood = dbConnect.connect().createArrayOf("TEXT", Food.convertToStringArr());

                // Prepare and execute the SQL statement that will update the users diet log
                PreparedStatement st = dbConnect.connect().prepareStatement("UPDATE userDB SET food = ? WHERE userName = ?");
                st.setArray(1, dbFood);
                st.setString(2, currentUser.getUserName());
                st.executeUpdate();
                st.close();
            }

                // Redirect back to the dashboard
                response.sendRedirect("/dashboard.jsp");

        } catch (SQLException e) {

            e.printStackTrace();
        }
    }
}