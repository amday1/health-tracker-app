package Controller;

import Model.CurrentUser;
import Model.Goal;
import Model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet(name = "deleteGoal", urlPatterns = {"/deleteGoal"})
public class deleteGoal extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {

            // Store the current signed in user in a variable
            User currentUser = CurrentUser.getCurrentUser();

            // Get the current users goal list and store it
            ArrayList<Goal> userGoals = currentUser.getGoals();

            if (!userGoals.isEmpty()) {
                // When the goal with the matching goal ID is found then remove it
                for (int i = 0; i < userGoals.size(); i++) {
                    if (userGoals.get(i).getGoalID() == Integer.parseInt(request.getParameter("goalID"))) {
                        userGoals.remove(i);
                    }
                }

                // Set the current users goal list to be the modified version of the list
                currentUser.setGoals(userGoals);

                // Create an SQL array using the current users goal list
                Array dbGoals = dbConnect.connect().createArrayOf("TEXT", Goal.convertToStringArr());

                // Prepare and execute the SQL statement that will update the users diet log
                PreparedStatement st = dbConnect.connect().prepareStatement("UPDATE userDB SET goals = ? WHERE userName = ?");
                st.setArray(1, dbGoals);
                st.setString(2, currentUser.getUserName());
                st.executeUpdate();
                st.close();
            }

            // Redirect back to the dashboard
            response.sendRedirect("/dashboard.jsp");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}