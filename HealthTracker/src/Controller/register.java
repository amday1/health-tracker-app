package Controller;

import Model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static java.lang.System.out;

@WebServlet(name = "register", urlPatterns = {"/register"})
public class register extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            // If any of the fields are empty
            if (request.getParameter("userName").isEmpty() || request.getParameter("password").isEmpty() ||
                    request.getParameter("checkPassword").isEmpty() || request.getParameter("email").isEmpty() ||
                    request.getParameter("fullName").isEmpty() || request.getParameter("weight").isEmpty() ||
                    request.getParameter("height").isEmpty()) {
                // Print out an error message and refresh the page to inform the user
                String emptyRegister = "All the fields must filled out before submitting!";
                request.getSession().setAttribute("emptyRegister", emptyRegister);
                response.sendRedirect("/register.jsp");
            }
            // If the email entered is not valid
            else if (!User.validate(request.getParameter("email"))) {
                // Print out an error message and refresh the page to inform the user
                String invalidEmail = "The email you entered was invalid! Please try again!";
                request.getSession().setAttribute("invalidEmail", invalidEmail);
                response.sendRedirect("/register.jsp");
            }
            // If the passwords entered do not match
            else if (!request.getParameter("password").equals(request.getParameter("checkPassword"))) {
                // Print out an error message and refresh the page to inform the user
                String passwordMismatch = "The passwords you entered did not match! Please try again!";
                request.getSession().setAttribute("passwordMismatch", passwordMismatch);
                response.sendRedirect("/register.jsp");
            } else {
                // Prepare and execute the SQL statement that will get all the users from the database
                PreparedStatement usernameCheck = dbConnect.connect().prepareStatement("SELECT username FROM userdb");
                ResultSet usernameCheckResult = usernameCheck.executeQuery();
                // Loop through all the username's in the result set
                while (usernameCheckResult.next()) {
                    // If the username already exists in the database
                    if (usernameCheckResult.getString(1).equals(request.getParameter("userName"))) {
                        // Print out an error message and refresh the page to inform the user
                        String takenUsername = "The username you entered already exists! Try signing in!";
                        request.getSession().setAttribute("takenUsername", takenUsername);
                        response.sendRedirect("/register.jsp");
                        return;
                    }
                }
                usernameCheck.close();

                // Prepare and execute the SQL statement that will get all the emails from the database
                PreparedStatement emailCheck = dbConnect.connect().prepareStatement("SELECT email FROM userdb");
                ResultSet emailCheckResult = emailCheck.executeQuery();
                // Loop through all the email's in the result set
                while (emailCheckResult.next()) {
                    // If the email already exists in the database
                    if (emailCheckResult.getString(1).equals(request.getParameter("email"))) {
                        // Print out an error message and refresh the page to inform the user
                        String takenEmail = "The email you entered is already registered with an account! Try signing in!";
                        request.getSession().setAttribute("takenEmail", takenEmail);
                        response.sendRedirect("/register.jsp");
                        return;
                    }
                }
                emailCheck.close();
                double weight = Double.parseDouble(request.getParameter("weight"));
                // Prepare and execute the SQL statement that will add the user to the database
                PreparedStatement st = dbConnect.connect().prepareStatement("INSERT INTO userDB VALUES (?,?,?,?,?,?,?,?)");
                st.setString(1, request.getParameter("userName"));
                st.setString(2, User.generateSHAPassword(request.getParameter("password")));
                st.setBoolean(3, Boolean.valueOf(request.getParameter("gender")));
                st.setString(4, request.getParameter("email"));
                st.setString(5, request.getParameter("fullName"));
                st.setDouble(6, Double.parseDouble(request.getParameter("height")));
                st.setDouble(7, weight);
                st.setDouble(8, weight);
                st.executeUpdate();
                st.close();

                // Redirect back to the homepage
                response.sendRedirect("/");

            }
        } catch (Throwable e) {
            out.println(e);
        }
    }
}