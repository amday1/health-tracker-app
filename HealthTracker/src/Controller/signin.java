package Controller;

import Model.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

@WebServlet(name = "signin", urlPatterns = {"/signin"})
public class signin extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {

            // Get the username they are attempting to log in to
            String usernameAttempt = request.getParameter("userName");

            // If the username field is not blank AND the password field is not blank
            if (!usernameAttempt.isEmpty() && !request.getParameter("password").isEmpty()) {
                // generate the hashed password from the input
                String hashedPass = User.generateSHAPassword(request.getParameter("password"));

                // Prepare and execute the SQL statement that will get all the users from the database
                PreparedStatement st = dbConnect.connect().prepareStatement("SELECT username FROM userdb",
                        ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                ResultSet rs = st.executeQuery();

                // Prepare and execute the SQL statement that will get the number of users in the database
                PreparedStatement stCount = dbConnect.connect().prepareStatement("SELECT COUNT(username) FROM userdb", ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                ResultSet rsCount = stCount.executeQuery();
                rsCount.next();

                // This is the amount of users in the database
                int userCount = rsCount.getInt(1);

                // If the user count is 0 then that the user they entered definitely doesn't exist
                if (userCount == 0) {
                    // Print out an error message and refresh the page to inform the user
                    String invalidUser = "Sorry, that user that doesn't exist!";
                    request.getSession().setAttribute("invalidUser", invalidUser);
                    response.sendRedirect("/");
                    return;
                }

                // Declare a counter to count how many users we have looked through
                int currentUserCounter = 1;

                // Loop through the result set of users
                while (rs.next()) {
                    // If the username equals the attempted username
                    if (rs.getString(1).equals(usernameAttempt)) {
                        // Prepare and execute the SQL statement that will get password for that user from the database
                        PreparedStatement passCheck = dbConnect.connect().prepareStatement("SELECT pass FROM userdb WHERE username = ?");
                        passCheck.setString(1, usernameAttempt);
                        ResultSet passCheckResult = passCheck.executeQuery();

                        // Iterate to the password
                        passCheckResult.next();

                        // If the password from the database equals the password they entered
                        if (passCheckResult.getString("pass").equals(hashedPass)) {

                            // Prepare and execute the SQL statement that will get all that users details from the database
                            PreparedStatement row = dbConnect.connect().prepareStatement("SELECT * FROM userdb WHERE username = ?");
                            row.setString(1, usernameAttempt);
                            ResultSet rowResult = row.executeQuery();

                            // Position the result set properly
                            rowResult.next();

                            // Create an SQL array from the row result array
                            Array dbGoals = rowResult.getArray("goals");
                            // Create an empty ArrayList ready to store the Goal objects
                            ArrayList<Goal> goalList = new ArrayList<>();
                            // If it is not null
                            if (dbGoals != null) {
                                // Convert the SQL array to a Java string array
                                String[] goalArr = (String[]) dbGoals.getArray();
                                // Loop through the string array in increments of 4
                                for (int i = 0; i < goalArr.length; i += 4) {
                                    // Assign the goalID
                                    int goalID = Integer.parseInt(goalArr[i]);
                                    // Assign the actual goal
                                    String theGoal = goalArr[i + 1];
                                    // Get the date added as a string
                                    String dateAdded = goalArr[i + 2];
                                    // Format the date and parse it into a LocalDateTime object and then assign it
                                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d MMM yyyy  HH:mm");
                                    LocalDateTime dateTime = LocalDateTime.parse(dateAdded, formatter);
                                    // Assign the completed variable
                                    boolean completed = Boolean.parseBoolean(goalArr[i + 3]);

                                    // Create the new Goal object
                                    Goal newGoal = new Goal(goalID, theGoal, dateTime, completed);

                                    // Add the new Goal to the goal list
                                    goalList.add(newGoal);
                                }
                            }

                            // Create an SQL array from the row result array
                            Array dbExercise = rowResult.getArray("exercise");
                            // Create an empty ArrayList ready to store the Exercise objects
                            ArrayList<Exercise> exerciseList = new ArrayList<>();
                            // If it is not null
                            if (dbExercise != null) {
                                // Convert the SQL array to a Java string array
                                String[] exerciseArr = (String[]) dbExercise.getArray();
                                // Loop through the string array in increments of 5
                                for (int i = 0; i < exerciseArr.length; i += 5) {
                                    // Assign the exercise ID
                                    int exerciseID = Integer.parseInt(exerciseArr[i]);
                                    // Assign the exercise type
                                    String exerciseType = exerciseArr[i + 1];
                                    // Assign the duration
                                    int duration = Integer.parseInt(exerciseArr[i + 2]);
                                    // Assign the calories
                                    int calories = Integer.parseInt(exerciseArr[i + 3]);
                                    // Get the date added as a string
                                    String dateAdded = exerciseArr[i + 4];
                                    // Format the date and parse it into a LocalDateTime object and then assign it
                                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d MMM yyyy  HH:mm");
                                    LocalDateTime dateTime = LocalDateTime.parse(dateAdded, formatter);

                                    // Create the new Exercise object
                                    Exercise newExercise = new Exercise(exerciseID, exerciseType, duration, calories, dateTime);

                                    // Add the new Exercise to the goal list
                                    exerciseList.add(newExercise);
                                }
                            }

                            // Create an SQL array from the row result array
                            Array dbFood = rowResult.getArray("food");
                            // Create an empty ArrayList ready to store the Food objects
                            ArrayList<Food> foodList = new ArrayList<>();
                            // If it is not null
                            if (dbFood != null) {
                                // Convert the SQL array to a Java string array
                                String[] foodArr = (String[]) dbFood.getArray();
                                // Loop through the string array in increments of 4
                                for (int i = 0; i < foodArr.length; i += 4) {
                                    // Assign the food ID
                                    int foodID = Integer.parseInt(foodArr[i]);
                                    // Assign the food name
                                    String name = foodArr[i + 1];
                                    // Assign the calories
                                    int calories = Integer.parseInt(foodArr[i + 2]);
                                    // Get the date added as a string
                                    String dateEaten = foodArr[i + 3];
                                    // Format the date and parse it into a LocalDateTime object and then assign it
                                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d MMM yyyy  HH:mm");
                                    LocalDateTime dateTime = LocalDateTime.parse(dateEaten, formatter);

                                    // Create the new Food object
                                    Food newFood = new Food(foodID, name, calories, dateTime);

                                    // Add the new Food to the goal list
                                    foodList.add(newFood);
                                }
                            }

                            // Create the new user object using row result
                            User newUser = new User(rowResult.getString("userName"), request.getParameter("password"),
                                    rowResult.getBoolean("gender"), rowResult.getString("email"),
                                    rowResult.getString("fullName"), rowResult.getDouble("height"),
                                    rowResult.getDouble("startWeight"), rowResult.getDouble("currentWeight"),
                                    rowResult.getBoolean("metric"), goalList, exerciseList, foodList);

                            // Set the currently signed in user to the user just created
                            CurrentUser.setCurrentUser(newUser);
                            row.close();

                            // Redirect back to the dashboard
                            response.sendRedirect("/dashboard.jsp");
                            return;
                        }
                        // Password was wrong
                        else {
                            // Print out an error message and refresh the page to inform the user
                            String incorrectPassword = "The password you entered was incorrect for that user!";
                            request.getSession().setAttribute("incorrectPassword", incorrectPassword);
                            response.sendRedirect("/");
                            return;
                        }
                    }
                    // The username entered does not match the current user or it does not exist
                    else {
                        // If we are not at the last user
                        if (currentUserCounter != userCount) {
                            currentUserCounter++;
                        }
                        // At the last user and still not found so doesn't exist
                        else {
                            // Print out an error message and refresh the page to inform the user
                            String invalidUser = "Sorry, that user that doesn't exist!";
                            request.getSession().setAttribute("invalidUser", invalidUser);
                            response.sendRedirect("/");
                            return;
                        }
                    }
                }
                st.close();
            }
            // Either the username or password field was left blank
            else {
                // Print out an error message and refresh the page to inform the user
                String emptySignin = "Both fields must be filled out!";
                request.getSession().setAttribute("emptySignin", emptySignin);
                response.sendRedirect("/");
            }
        } catch (Throwable e) {
            System.out.println(e);
        }
    }
}