package Controller;

import Model.CurrentUser;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "signout", urlPatterns = {"/signout"})
public class signout extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            // Set the currently signed user to null
            CurrentUser.setCurrentUser(null);

            // Redirect to the homepage
            response.sendRedirect("/");

        } catch (Throwable e) {
            System.out.println(e);
        }
    }
}
