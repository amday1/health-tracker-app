package Controller;

import Model.CurrentUser;
import Model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;

@WebServlet(name = "updateDetails", urlPatterns = {"/updateDetails"})
public class updateDetails extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {

            // Store the current signed in user in a variable
            User currentUser = CurrentUser.getCurrentUser();

            // If the height input box is not empty
            if (!request.getParameter("height").isEmpty()) {
                // Set the height of the current user to the new height
                currentUser.setHeight(Double.parseDouble(request.getParameter("height")));
            }

            // If the weight input box is not empty
            if (!request.getParameter("weight").isEmpty()) {
                // Set the height of the current user to the new weight
                currentUser.setCurrentWeight(Double.parseDouble(request.getParameter("weight")));
            }

            // Prepare and execute the SQL statement that will update the users height and weight
            PreparedStatement st = dbConnect.connect().prepareStatement("UPDATE userDB SET height = ?, currentWeight = ? WHERE userName = ?");
            st.setDouble(1, currentUser.getHeight());
            st.setDouble(2, currentUser.getCurrentWeight());
            st.setString(3, currentUser.getUserName());
            st.executeUpdate();
            st.close();

            // Update the users bmi
            currentUser.setBmi();

            // Redirect to the dashboard
            response.sendRedirect("/dashboard.jsp");

        } catch (Throwable e) {
            System.out.println(e);
        }
    }
}