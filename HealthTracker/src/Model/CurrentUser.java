package Model;

public class CurrentUser {

    // Initialises the current user as null as no one is signed in
    private static User currentUser = null;

    /**
     * Accessor method to get the current user
     *
     * @return the current user that is signed in
     * @author AMD
     */
    public static User getCurrentUser() {
        return currentUser;
    }

    /**
     * Method to set a new current user
     *
     * @param newCurrentUser the user that is now signed in
     * @author AMD
     */
    public static void setCurrentUser(User newCurrentUser) {
        currentUser = newCurrentUser;
    }
}