package Model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Exercise {

    // Declare the fields that make up an exercise object
    private int exerciseID;
    private String exerciseType;
    private int duration;
    private int calories;
    private LocalDateTime dateCompleted;

    /**
     * Constructor for an exercise object
     *
     * @param exerciseType  the name of the exercise
     * @param duration      how long the exercise was done for
     * @param calories      the calories burned when doing the exercise
     * @param dateCompleted when the exercise was completed
     * @author AMD
     */
    public Exercise(int exerciseID, String exerciseType, int duration, int calories, LocalDateTime dateCompleted) {
        this.setExerciseID(exerciseID);
        this.exerciseType = exerciseType;
        this.duration = duration;
        this.calories = calories;
        this.setDateCompleted(dateCompleted);
    }

    /**
     * @return type of exercise
     * @author AMD
     */
    public String getExerciseType() {
        return exerciseType;
    }

    /**
     * set exercise type
     *
     * @param exerciseType the name of the exercise
     * @author AMD
     */
    public void setExerciseType(String exerciseType) {
        this.exerciseType = exerciseType;
    }

    /**
     * Method to get the duration of the exericse
     *
     * @return duration
     * @author AMD
     */
    public int getDuration() {
        return duration;
    }

    /**
     * Accessor method for calories
     *
     * @return calories
     * @author AMD
     */
    public int getCalories() {
        return calories;
    }

    /**
     * set number of calories
     *
     * @param calories
     * @author AMD
     */
    public void setCalories(int calories) {
        this.calories = calories;
    }

    /**
     * Accessor method that returns the date completed for the exercise
     *
     * @return the date completed
     * @author Seb Papararo
     */
    public LocalDateTime getDateCompleted() {
        return dateCompleted;
    }

    /**
     * Mutator method that sets the date completed
     *
     * @param dateCompleted the new date it was completed
     * @author Seb Papararo
     */
    public void setDateCompleted(LocalDateTime dateCompleted) {
        this.dateCompleted = dateCompleted;
    }

    /**
     * Accessor method that returns the exercises ID
     *
     * @return the exercises ID
     * @author Seb Papararo
     */
    public int getExerciseID() {
        return exerciseID;
    }

    /**
     * Mutator method that sets the exercise ID
     *
     * @param exerciseID the new exercise ID
     * @author Seb Papararo
     */
    public void setExerciseID(int exerciseID) {
        this.exerciseID = exerciseID;
    }

    /**
     * toString method to output an exercise
     *
     * @return exercise object as a string
     * @author AMD
     */
    public String toString() {
        StringBuilder str = new StringBuilder("Exercise ID: ").append(getExerciseID()).append("<br>");
        str.append("Exercise Type: ").append(getExerciseType()).append("<br>");
        str.append("Duration (mins): ").append(getDuration()).append("<br>");
        str.append("Calories burned: ").append(getCalories()).append("<br>");
        str.append("Date completed: ");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d MMM yyyy" +
                "  HH:mm");
        String formattedDateTime = getDateCompleted().format(formatter);
        str.append(formattedDateTime).append("<br>");
        return str.toString();
    }

    /**
     * A method that converts the ArrayList exercise log to a String array
     *
     * @return A string array version the exercise log
     * @author Seb Papararo
     */
    public static String[] convertToStringArr() {
        User currentUser = CurrentUser.getCurrentUser();
        // Store the exercise log as array of strings
        String[] exerciseLog = new String[currentUser.getExerciseLog().size() * 5];
        int count = 0;
        for (Exercise e : currentUser.getExerciseLog()) {
            exerciseLog[count] = String.valueOf(e.getExerciseID());
            count++;
            exerciseLog[count] = e.getExerciseType();
            count++;
            exerciseLog[count] = String.valueOf(e.getDuration());
            count++;
            exerciseLog[count] = String.valueOf(e.getCalories());
            count++;
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d MMM yyyy  HH:mm");
            String formattedDateTime = e.getDateCompleted().format(formatter);
            exerciseLog[count] = formattedDateTime;
            count++;
        }
        return exerciseLog;
    }
}
