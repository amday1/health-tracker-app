package Model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Food {

    // Declare the fields used in a Food object
    private int foodID;
    private String name;
    private int calories;
    private LocalDateTime dateEaten;

    /**
     * Constructor for a Food object
     *
     * @param name      the name of the food
     * @param calories  the amount eaten in calories
     * @param dateEaten the date eaten the food
     * @author AMD
     */
    public Food(int foodID, String name, int calories, LocalDateTime dateEaten) {
        this.setFoodID(foodID);
        this.name = name;
        this.calories = calories;
        this.setDateEaten(dateEaten);
    }

    /**
     * Accessor method that returns food name
     *
     * @return the foods name
     * @author AMD
     */
    public String getName() {
        return name;
    }

    /**
     * Mutator method to set name of food
     *
     * @param name the new name of the food
     * @author AMD
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Accessor method that gets the calories of a food
     *
     * @return calories
     * @author AMD
     */
    public int getCalories() {
        return calories;
    }

    /**
     * Mutator method that sets the number of calories in a food
     *
     * @param calories the number of calories for that food
     * @author AMD
     */
    public void setCalories(int calories) {
        this.calories = calories;
    }

    /**
     * Accessor method that gets the time eaten for food
     *
     * @return time eaten
     * @author AMD
     */
    public LocalDateTime getDateEaten() {
        return dateEaten;
    }

    /**
     * Mutator method that sets the time eaten for food
     *
     * @param dateEaten the time that the food was eaten
     * @author AMD
     */
    public void setDateEaten(LocalDateTime dateEaten) {
        this.dateEaten = dateEaten;
    }

    /**
     * Accessor method that gets the foods ID
     *
     * @return the foods ID
     * @author Seb Papararo
     */
    public int getFoodID() {
        return foodID;
    }

    /**
     * Mutator method that sets the foods ID
     *
     * @param foodID the foods ID
     * @author Seb Papararo
     */
    public void setFoodID(int foodID) {
        this.foodID = foodID;
    }

    /**
     * toString method to output a food
     *
     * @return a Food obect in string form
     * @author AMD
     */
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder("Food ID: ").append(getFoodID()).append("<br>");
        str.append("Food Name: ").append(getName()).append("<br>");
        str.append("Calories: ").append(getCalories()).append("<br>");
        str.append("Date eaten: ");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d MMM yyyy" +
                "  HH:mm");
        String formattedDateTime = getDateEaten().format(formatter);
        str.append(formattedDateTime).append("<br>");
        return str.toString();
    }

    /**
     * A method that converts the ArrayList diet log to a String array
     *
     * @return A string array version the diet log
     * @author Seb Papararo
     */
    public static String[] convertToStringArr() {
        User currentUser = CurrentUser.getCurrentUser();
        // Store the exercise log as array of strings
        String[] foodLog = new String[currentUser.getDietLog().size() * 4];
        int count = 0;
        for (Food f : currentUser.getDietLog()) {
            foodLog[count] = String.valueOf(f.getFoodID());
            count++;
            foodLog[count] = f.getName();
            count++;
            foodLog[count] = String.valueOf(f.getCalories());
            count++;
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d MMM yyyy  HH:mm");
            String formattedDateTime = f.getDateEaten().format(formatter);
            foodLog[count] = formattedDateTime;
            count++;
        }
        return foodLog;
    }
}
