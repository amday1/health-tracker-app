package Model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Goal {

    // Declare the fields that make up a goal object
    private int goalID;
    private String goal;
    private LocalDateTime dateAdded;
    private boolean completed;

    /**
     * A constructor for creating a goal object
     *
     * @param goalID    the ID of the goal
     * @param goal      the goal itself
     * @param dateAdded the date the goal was added
     * @param completed whether the goal is completed or not
     * @author Seb Papararo
     */
    public Goal(int goalID, String goal, LocalDateTime dateAdded, boolean completed) {
        this.goalID = goalID;
        this.goal = goal;
        this.dateAdded = dateAdded;
        this.setCompleted(completed);
    }

    /**
     * Accessor method that gets the goal ID
     *
     * @return the goal ID
     * @author Seb Papararo
     */
    public int getGoalID() {
        return goalID;
    }

    /**
     * Accessor method that gets the goal itself
     *
     * @return the goal
     * @author Seb Papararo
     */
    public String getGoal() {
        return goal;
    }

    /**
     * Accessor method that returns the date it was added
     *
     * @return the date added
     * @author Seb Papararo
     */
    public LocalDateTime getDateAdded() {
        return dateAdded;
    }

    /**
     * Accessor method that gets whether a goal is competed or not
     *
     * @return completed?
     * @author Seb Papararo
     */
    public boolean isCompleted() {
        return completed;
    }

    /**
     * Mutator method that sets whether a goal is completed to the new value
     *
     * @param completed the new value of completed
     * @author Seb Papararo
     */
    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    /**
     * A method that prints out a goal object as a string
     *
     * @return a goal obejct in string form
     * @author Seb Papararo
     */
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder("Goal ID: ").append(getGoalID());
        str.append("<br>").append("The Goal: ");
        str.append(getGoal()).append("<br>");
        str.append("Date added: ");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d MMM yyyy  HH:mm");
        String formattedDateTime = getDateAdded().format(formatter);
        str.append(formattedDateTime).append("<br>");
        str.append("Completed: ").append(isCompleted()).append("<br>");
        return str.toString();
    }

    /**
     * A method that converts the ArrayList goal list to a String array
     *
     * @return A string array version the goal list
     * @author Seb Papararo
     */
    public static String[] convertToStringArr() {
        User currentUser = CurrentUser.getCurrentUser();
        String[] goalLog = new String[currentUser.getGoals().size() * 4];
        int count = 0;
        for (Goal g : currentUser.getGoals()) {
            goalLog[count] = String.valueOf(g.getGoalID());
            count++;
            goalLog[count] = g.getGoal();
            count++;
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern
                    ("d MMM yyyy  HH:mm");
            String formattedDateTime = g.getDateAdded().format(formatter);
            goalLog[count] = formattedDateTime;
            count++;
            goalLog[count] = String.valueOf(g.isCompleted());
            count++;
        }
        return goalLog;
    }
}