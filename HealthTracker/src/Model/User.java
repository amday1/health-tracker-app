package Model;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class User {

    // Declare the fields that make up a user object
    private String userName;
    private String password;
    private boolean gender;
    private String email;
    private String fullName;
    private double height;
    private double startWeight;
    private double currentWeight;
    private double bmi;
    private ArrayList<Goal> goals;
    private ArrayList<Food> dietLog;
    private ArrayList<Exercise> exerciseLog;
    private boolean metric = true;


    /**
     * A constructor that creates a user object
     *
     * @param userName the username of the account
     * @param password the password of the account
     * @param gender   the users gender
     * @param email    the users email
     * @param fullName the users full name
     * @param height   the users height
     * @param startWeight   the users start weight
     * @param currentWeight the users current weight
     * @param metric   what unit type the users wants to use
     * @param goals    the users goals
     * @param exercise the users completed exercise
     * @param foodLog  the users diet log
     * @author Seb Papararo
     */
    public User(String userName, String password, boolean gender, String email, String fullName, double
            height, double startWeight, double currentWeight, boolean metric, ArrayList<Goal> goals,
                ArrayList<Exercise> exercise, ArrayList<Food> foodLog) {
        this.userName = userName;
        this.password = generateSHAPassword(password);
        this.gender = gender;
        this.setEmail(email);
        this.fullName = fullName;
        this.setStartWeight(startWeight);
        this.setCurrentWeight(currentWeight);
        this.setHeight(height);
        this.metric = metric;
        setBmi();
        this.setGoals(goals);
        this.setDietLog(foodLog);
        this.setExerciseLog(exercise);
    }

    /**
     * Method to generate a SHA-512 password to store in the database
     *
     * @param passwordToHash the string password
     * @return the hashed password
     * @author AMD
     */
    public static String generateSHAPassword(String passwordToHash) {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");

            byte[] bytes = md.digest(passwordToHash.getBytes());
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }

    /**
     * Simple accessor method that returns a persons username
     *
     * @return the persons username
     * @author Seb Papararo
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Simple accessor method that returns a users password
     *
     * @return the users password
     * @author AMD
     */
    public String getPassword() {
        return password;
    }

    /**
     * Simple accessor method that returns a persons gender where male = true and female = false
     *
     * @return the persons gender
     * @author AMD
     */
    public boolean isGender() {
        return gender;
    }

    /**
     * Simple accessor method that returns a persons email
     *
     * @return the persons email
     * @author Seb Papararo
     */
    public String getEmail() {
        return email;
    }

    /**
     * Simple accessor method that returns a persons name
     *
     * @return the persons name
     * @author Seb Papararo
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * Simple accessor method that returns a persons height
     *
     * @return the persons height
     * @author Seb Papararo
     */
    public double getHeight() {
        return height;
    }

    /**
     * Simple accessor method that returns a persons start weight
     *
     * @return the persons weight
     * @author Seb Papararo
     */
    public double getStartWeight() {
        return startWeight;
    }

    /**
     * Simple accessor method that returns a persons current weight
     *
     * @return the persons weight
     * @author Seb Papararo
     */
    public double getCurrentWeight() {
        return currentWeight;
    }

    /**
     * Simple accessor method that returns a persons bmi
     *
     * @return the persons bmi
     * @author Seb Papararo
     */
    public double getBmi() {
        return bmi;
    }

    /**
     * Used to round a double value to 1 decimal place
     *
     * @param value     the figure to be rounded
     * @param precision what to round the value to
     * @return rounded value
     * @author AMD
     */
    public double roundDouble(double value, int precision) {
        int scale = (int) Math.pow(10, precision);
        return (double) Math.round(value * scale) / scale;
    }

    /**
     * Simple mutator method that returns a persons bmi
     *
     * @author Seb Papararo
     */
    public void setBmi() {
        if (metric) {
            bmi = currentWeight / Math.pow(height / 100, 2);
        } else {
            bmi = (currentWeight * 703) / Math.pow(height, 2);
        }
    }

    /**
     * Simple accessor method that returns a persons goals
     *
     * @return the persons goals
     * @author Seb Papararo
     */
    public ArrayList<Goal> getGoals() {
        return goals;
    }

    /**
     * Simple accessor method that returns a persons diet log
     *
     * @return the persons diet log
     * @author Seb Papararo
     */
    public ArrayList<Food> getDietLog() {
        return dietLog;
    }

    /**
     * Simple accessor method that returns a persons exercise log
     *
     * @return the persons exercise log
     * @author Seb Papararo
     */
    public ArrayList<Exercise> getExerciseLog() {
        return exerciseLog;
    }


    /**
     * Accessor method to get whether user is in metric or not
     *
     * @return true/false
     * @author AMD
     */
    public boolean getMetric() {
        return metric;
    }

    /**
     * Simple mutator method that sets the unit type
     * True for metric, false for imperial
     *
     * @param unitType
     * @author Seb Papararo
     */
    public void setMetric(boolean unitType) {
        // No change needed
        if (unitType == metric) {
            System.out.println("The unit type you selected is already the " +
                    "the default");
        } else {
            metric = unitType;
            convertUnits(unitType);
        }
    }

    /**
     * Method to set the users email
     *
     * @param email the users email
     * @author Seb Papararo
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Method to set the users height
     *
     * @param height the users height
     * @author Seb Papararo
     */
    public void setHeight(double height) {
        this.height = height;
    }

    /**
     * Method to set the users weight
     *
     * @param startWeight the users weight
     * @author Seb Papararo
     */
    public void setStartWeight(double startWeight) {
        this.startWeight = startWeight;
    }

    /**
     * Method to set the users weight
     *
     * @param currentWeight the users weight
     * @author Seb Papararo
     */
    public void setCurrentWeight(double currentWeight) {
        this.currentWeight = currentWeight;
    }

    /**
     * Mutator method that sets the users list of goals
     *
     * @param goals the users goal list
     * @author Seb Papararo
     */
    public void setGoals(ArrayList<Goal> goals) {
        this.goals = goals;
    }

    /**
     * Mutator method that sets the users diet log
     *
     * @param dietLog the users diet log
     * @author Seb Papararo
     */
    public void setDietLog(ArrayList<Food> dietLog) {
        this.dietLog = dietLog;
    }

    /**
     * Mutator method that sets the users exercise log
     *
     * @param exerciseLog the users exercise log
     * @author Seb Papararo
     */
    public void setExerciseLog(ArrayList<Exercise> exerciseLog) {
        this.exerciseLog = exerciseLog;
    }

    /**
     * Mutator method that converts all values to the new unit type
     *
     * @param unitType the type of the units, metric or imperial
     */
    public void convertUnits(boolean unitType) {
        // Converting to metric
        if (unitType) {
            // Converting the height
            double metHeight = height / 0.39370;
            setHeight(metHeight);

            // Converting the weight
            double metWeight = currentWeight / 2.20462;
            setCurrentWeight(metWeight);
            double metStartWeight = startWeight / 2.20462;
            setStartWeight(metStartWeight);

            // Recalculate the bmi
            //setBmi();
        }
        // Converting to imperial
        else {
            // Converting the height
            double impHeight = height / 2.54;
            setHeight(impHeight);

            // Converting the weight
            double impWeight = currentWeight / 0.453592;
            setCurrentWeight(impWeight);
            double impStartWeight = startWeight / 0.453592;
            setStartWeight(impStartWeight);

            // Recalculate the bmi
            //setBmi();
        }
    }

    /**
     * Method that calcualtes the weight difference between the current weight and the starting weight
     *
     * @return
     * @author Adam Loraine
     */
    public double weightDifference() {
        return startWeight - currentWeight;
    }

    // Regular expression used in the email validation to check it matches the format
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
            Pattern.CASE_INSENSITIVE);

    /**
     * Method to utilise email pattern above
     *
     * @param emailStr the users email
     * @return true if the email validates correctly, false otherwise
     */
    public static boolean validate(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }
}