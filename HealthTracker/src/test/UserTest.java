package test;

import Model.Exercise;
import Model.Food;
import Model.Goal;
import Model.User;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class UserTest {
    // create model class for testing
    ArrayList<Goal> goalList = new ArrayList<>();
    ArrayList<Food> foodList = new ArrayList<>();
    ArrayList<Exercise> exerciseList = new ArrayList<>();
    User testUser = new User("test", "test", true, "test@test.com",
            "test test", 100, 100, 100, true,
            goalList, exerciseList, foodList);

    // test that password hashes correctly.
    @Test
    public void testGenerateSHAPassword() throws Exception {
        // this string is an externally produced SHA hash for the password "test"
        String passHashed = "ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27a" +
                "c185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff";
        assertTrue("Password not encrypting to SHA-512 properly", passHashed.equals(testUser.getPassword()));
    }

    // this that BMI calculated correctly for metric units.
    @Test
    public void testGetBmi1() throws Exception {
        // calculate bmi given weight and height of 100.
        double bmiPredicted = 100 / Math.pow(100 / 100, 2);
        assertEquals("BMI is not calculated correctly for metric units", bmiPredicted, testUser.getBmi(),
                0.1);
    }

    // this that BMI calculated correctly for imperial units.
    @Test
    public void testGetBmi2() throws Exception {
        double heightImperial = 39.3701;
        double weightImperial = 220.462;
        testUser.convertUnits(false);

        // calculate bmi given weight and height of 100.
        double bmiPredicted = (weightImperial * 703) / Math.pow(heightImperial, 2);
        assertEquals("BMI is not calculated correctly for imperial units", bmiPredicted, testUser.getBmi(),
                0.1);

    }

    @Test
    public void testRoundDouble() throws Exception {
        double testDouble = testUser.roundDouble(123.4567889, 1);
        double expectedRound = 123.5;
        assertEquals(expectedRound, testDouble, 0);
    }

    @Test
    public void testConvertUnits() throws Exception {
        double imperialHeight = 39.3701;
        testUser.convertUnits(false);
        assertEquals(imperialHeight, testUser.getHeight(), 0.1);
    }

    @Test
    public void testWeightDifference() throws Exception {
        assertEquals(testUser.getStartWeight() - testUser.getCurrentWeight(), 0, 0.1);
    }

    @Test
    public void testWeightDifference2() throws Exception {
        testUser.setStartWeight(100);
        testUser.setCurrentWeight(105);
        assertEquals(testUser.getStartWeight() - testUser.getCurrentWeight(), -5, 0.1);
    }

    // Test the email with an email that should be valid
    @Test
    public void testEmailValidation() throws Exception {
        assertTrue("Email was valid, and was accepted!", User.validate(testUser.getEmail()));
    }

    // Test an email that is not valid
    @Test
    public void testEmailValidation2() throws Exception {
        String email = "abc.abc.abc.abc@abc";
        assertFalse("Email was not valid", User.validate(email));
    }
}
