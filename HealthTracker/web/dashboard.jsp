<%@ page import="Model.*" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%--
  Created by IntelliJ IDEA.
  User: sebpa
  Date: 12/03/2018
  Time: 17:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dashboard | FitTrack</title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <style>
        body {
            padding-top: 50px;
        }

        .starter-template {
            padding: 40px 15px;
            text-align: center;
        }
    </style>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/index.jsp">FitTrack</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="/register.jsp">Create Account</a></li>
                <li><a href="/">Sign In</a></li>

            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>

<div class="container">
    <div class="jumbotron">
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    <%
                        User currentUser = CurrentUser.getCurrentUser();

                        if (currentUser != null) {
                            out.println("<h1>Welcome to FitTrack, " + currentUser.getUserName() + "</h1>");
                            out.println("<h2> Your Info:</h2>");
                            out.println("<ul><li>Full name: " + currentUser.getFullName() + "</li>");
                            if (currentUser.isGender()) {
                                out.println("<li>Gender: Male</li>");
                            } else {
                                out.println("<li>Gender: Female</li>");
                            }
                            out.println("<li>Email: " + currentUser.getEmail() + "</li>");
                            if (currentUser.getMetric()) {
                                out.println("<li>Units: Metric </li>");
                            } else {
                                out.println("<li>Units: Imperial </li>");
                            }
                            if (currentUser.getMetric()) {
                                out.println("<li>Height: " +
                                        currentUser.roundDouble(currentUser.getHeight(), 1) + " cm" +
                                        "</li>");
                                out.println("<li>Weight: " +
                                        currentUser.roundDouble(currentUser.getCurrentWeight(), 1) + " kg" +
                                        "</li>");
                            } else {
                                out.println("<li>Height: " +
                                        currentUser.roundDouble(currentUser.getHeight(), 1) + " inches" +
                                        "</li>");
                                out.println("<li>Weight: " +
                                        currentUser.roundDouble(currentUser.getCurrentWeight(), 1) + " lbs" +
                                        "</li>");
                            }

                            out.println("<li>BMI: " + currentUser.roundDouble(currentUser.getBmi(),
                                    1) + "</li></ul><br>");
                        }
                    %>
                </div>

                <div class="col-xs-6">
                    <%
                        currentUser = CurrentUser.getCurrentUser();
                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern
                                ("d MMM yyyy");
                        int calorieCount = 0;
                        int exerciseCount = 0;
                        int exerciseCaloriesCount = 0;
                        double startWeight = 0;
                        double currentWeight = 0;

                        if (currentUser != null) {
                            out.println("<h2>Today's Stats</h2>");
                            out.println("<h3>Calories</h3>");

                            for (Food f : currentUser.getDietLog()) {
                                if (f.getDateEaten().format(formatter).equals(LocalDateTime.now().format(formatter))) {
                                    calorieCount += f.getCalories();
                                }
                            }
                            out.println("Total calories eaten: " + calorieCount + "<br>");

                            for (Exercise e : currentUser.getExerciseLog()) {
                                if (e.getDateCompleted().format(formatter).equals(LocalDateTime.now().format(formatter))) {
                                    exerciseCaloriesCount += e.getCalories();
                                }
                            }
                            out.println("Total calories burned: " + exerciseCaloriesCount + "<br>");

                            out.println("<h3>Exercise</h3>");
                            for (Exercise e : currentUser.getExerciseLog()) {
                                if (e.getDateCompleted().format(formatter).equals(LocalDateTime.now().format(formatter))) {
                                    exerciseCount += e.getDuration();
                                }
                            }
                            out.println("Total exercise in minutes: " + exerciseCount);

                            out.println("<h2>Weight Change</h2>");

                            double difference = currentUser.weightDifference();
                            DecimalFormat df2 = new DecimalFormat(".##");

                            if (currentUser.getMetric()) {
                                if (difference >= 0) {
                                    out.println("<p> " + df2.format(difference) + " kg lost.</p>");
                                } else {
                                    out.println("<p> " + df2.format(difference * -1) + " kg gained.</p>");
                                }
                            } else {
                                if (difference >= 0) {
                                    out.println("<p> " + df2.format(difference) + " lbs lost.</p>");
                                } else {
                                    out.println("<p> " + df2.format(difference * -1) + " lbs gained.</p>");
                                }
                            }
                        }
                    %>
                </div>
            </div>
            <form action="changeUnits" method="POST">
                Switch between imperial and metric units:
                <button type="submit" class="btn btn-info">Switch</button>
            </form>
            <form action="signout" method="POST">
                Click here to sign out:
                <button type="submit" class="btn btn-info">Sign Out</button>
            </form>
            </p>
        </div>
    </div>


    <div class="jumbotron">
        <div class="row">
            <div class="col-xs-6 col-md-4">
                <p>
                    <%
                        out.println("<h4>Goals</h4><br>");

                        for (Goal goal : currentUser.getGoals()) {
                            out.println(goal.toString() + "<br>");
                        }
                    %>
                </p>
            </div>
            <div class="col-xs-6 col-md-4">
                <p>
                    <%
                        out.println("<h4>Food eaten</h4><br>");

                        for (Food f : currentUser.getDietLog()) {
                            out.println(f.toString() + "<br>");
                        }
                    %>
                </p>
            </div>
            <div class="col-xs-6 col-md-4">
                <p>
                    <%
                        out.println("<h4>Exercise done</h4><br>");

                        for (Exercise e : currentUser.getExerciseLog()) {
                            out.println(e.toString() + "<br>");
                        }
                    %>
                </p>
            </div>
        </div>
    </div>

    <div class="jumbotron">
        <div class="row">
            <div class="col-xs-6">
                <h4>Add a goal</h4>
                <form action="addGoal" method="POST">
                    Add a goal: <input type="text" name="newGoal">
                    <button type="submit" class="btn btn-info">Submit</button>
                </form>
            </div>
            <div class="col-xs-6">
                <h4>Add food</h4>
                <form action="addFood" method="POST">
                    Food name: <input type="text" name="foodName"><br><br>
                    Calories eaten: <input type="number" name="foodCalories"><br><br>
                    <button type="submit" class="btn btn-info">Submit</button>
                </form>
            </div>
            <div class="col-xs-6">
                <h4>Add exercise</h4>
                <form action="addExercise" method="POST">
                    Exercise type e.g. running: <input type="text" name="exerciseType"><br><br>
                    Duration (mins): <input type="number" name="duration"><br><br>
                    Calories burned: <input type="number" name="exerciseCalories"><br><br>
                    <button type="submit" class="btn btn-info">Submit</button>
                </form>
            </div>
        </div>
    </div>


    <div class="jumbotron">
        <div class="row">
            <h4>Mark a goal as completed: </h4>
            <form action="completeGoal" method="POST">
                Enter the ID of the goal you wish to complete: <input type="number" name="goalID"><br>
                <button type="submit" class="btn btn-info">Submit</button>
            </form>
        </div>
    </div>

    <div class="jumbotron">
        <h4>Delete a Goal: </h4>
        <form action="deleteGoal" method="POST">
            Enter the ID of the goal you wish to delete: <input type="number" name="goalID"><br>
            <button type="submit" class="btn btn-info">Submit</button>
        </form>
        <h4>Delete a Food: </h4>
        <form action="deleteFood" method="POST">
            Enter the ID of the food you wish to delete: <input type="number" name="foodID"><br>
            <button type="submit" class="btn btn-info">Submit</button>
        </form>
        <h4>Delete an Exercise: </h4>
        <form action="deleteExercise" method="POST">
            Enter the ID of the goal you wish to delete: <input type="number" name="exerciseID"><br>
            <button type="submit" class="btn btn-info">Submit</button>
        </form>
    </div>

    <div class="jumbotron">
        <div class="row">
            <div class="col-xs-6">
                <h4>Update personal information</h4>
                <form action="updateDetails" method="POST">
                    <%
                        String h = "cm";
                        String w = "kg";
                        if (!currentUser.getMetric()) {
                            h = "in";
                            w = "lbs";
                        }
                        out.println("Enter your new height " + h + ": <input type=\"number\" name=\"height\"><br><br>");
                        out.println("Enter your new weight " + w + ": <input type=\"number\" name=\"weight\"><br><br>");
                    %>
                    <button type="submit" class="btn btn-info">Submit</button>
                </form>
                <br>
            </div>
        </div>
    </div>


    <div class="jumbotron">
        <h4>Change password</h4>
        <form action="changePassword" method="POST">
            Old password: <input type="password" name="oldAccPassword"><br><br>
            New password: <input type="password" name="newAccPassword"><br><br>
            <button type="submit" class="btn btn-info">Change</button>
        </form>
    </div>
    <div class="jumbotron">
        <h4>Delete your account</h4>
        <form action="deleteAccount" method="POST">
            Confirm password: <input type="password" name="deleteAccPassword"><br>
            <button type="submit" class="btn btn-info">Delete</button>
        </form>

        <p class="error-red">${incorrectPassword}</p>
        <c:remove var="incorrectPassword" scope="page"/>

        <%
            request.getSession().setAttribute("incorrectPassword", "");
        %>
    </div>
</div><!-- /.container -->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
