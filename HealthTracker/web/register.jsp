<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Create Account | FitTrack</title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <!-- Custom styles for this template -->
    <style>
        body {
            padding-top: 50px;
        }

        .starter-template {
            padding: 40px 15px;
            text-align: left;
        }

        .error-red {
            color: red;
        }

    </style>
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/index.jsp">FitTrack</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="/register.jsp">Create Account</a></li>
                <li><a href="/">Sign In</a></li>

            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>
<br><br>
<div class="container">
    <div class="row centered-form">
        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Sign up</h3>
                </div>
                <div class="panel-body">
                    <form action="/register" method="POST">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="text" name="userName" id="first_name" class="form-control input-sm"
                                           placeholder="Username">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="text" name="fullName" id="fullName" class="form-control input-sm"
                                           placeholder="Full Name">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="email" name="email" id="email" class="form-control input-sm"
                                   placeholder="Email Address">
                        </div>


                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                            <label class="btn btn-secondary active">
                                <input type="radio" name="gender" id="gender1" autocomplete="off" checked>Male
                            </label>
                            <label class="btn btn-secondary">
                                <input type="radio" name="gender" id="gender2" autocomplete="off">Female
                            </label>
                        </div>

                        <br>
                        <br>

                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="password" name="password" id="password" class="form-control input-sm"
                                           placeholder="Password">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="password" name="checkPassword" id="checkPassword"
                                           class="form-control input-sm" placeholder="Confirm Password">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="number" name="height" id="height" class="form-control input-sm"
                                           placeholder="Height e.g. 85.5cm">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="number" name="weight" id="weight" class="form-control input-sm"
                                           placeholder="Weight e.g. 170.5">
                                </div>
                            </div>
                        </div>

                        <%--<input type="submit" value="Register" class="btn btn-info btn-block">--%>
                        <button type="submit" class="btn btn-info btn-block">Register</button>
                        <%--<button type="submit" value="btn btn-primary">Create Account</button>--%>

                    </form>
                    <p class="error-red">${takenEmail}</p>
                    <c:remove var="takenEmail" scope="page"/>
                    <p class="error-red">${takenUsername}</p>
                    <c:remove var="takenUsername" scope="page"/>
                    <p class="error-red">${emptyRegister}</p>
                    <c:remove var="emptyRegister" scope="page"/>
                    <p class="error-red">${invalidEmail}</p>
                    <c:remove var="invalidEmail" scope="page"/>
                    <p class="error-red">${passwordMismatch}</p>
                    <c:remove var="passwordMismatch" scope="page"/>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<%
    request.getSession().setAttribute("passwordMismatch", "");
    request.getSession().setAttribute("emptyRegister", "");
    request.getSession().setAttribute("invalidEmail", "");
    request.getSession().setAttribute("takenUsername", "");
    request.getSession().setAttribute("takenEmail", "");
%>

</body>
</html>
