# HealthTracker (FitTrack) README
Simple health tracker web application which stores user information including username and secure log in.

User is able to see their daily statistics and change relevant information within the application such as adding a new weight, changing their password and add goals for them to complete.

Implemented in Java, JSP, and an AWS database.